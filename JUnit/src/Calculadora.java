public class Calculadora {

	private int calculo = 0;

	private String version;

	public Calculadora() {
		inicializar();
	}

	public void inicializar() {
		this.calculo = 0;
		this.version = "0.0";
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void sumar(int cantidad) {
		this.calculo += cantidad;
	}

	public void restar(int cantidad) {
		this.calculo -= cantidad;
	}

	public void dividir(int div) {
		this.calculo /= div;
	}

	public int getCalculo() {
		return this.calculo;
	}

	public String toString() {
		return "Calculo: " + this.getCalculo();

	}

	public static void main(String[] args) {
		Calculadora calculadora = new Calculadora();
		calculadora.sumar(100);
		calculadora.dividir(5);
		System.out.println(calculadora);

	}
}
