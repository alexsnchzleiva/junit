import junit.framework.TestCase;
import junit.textui.TestRunner;

public class CalculadoraTest extends TestCase {

	private Calculadora calculadora = null;

	public CalculadoraTest(String name) {
		super(name);
	}

	protected void setUp() throws Exception {
		super.setUp();
		calculadora = new Calculadora();
		calculadora.inicializar();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		calculadora = null;
	}

	public void testSuma() {
		calculadora.sumar(20);
		assertEquals(20, calculadora.getCalculo());
		assertTrue((calculadora.getCalculo() == 20));
	}

	public void testSumaCero() {
		calculadora.sumar(0);
		assertEquals(0, calculadora.getCalculo());
	}

	public void testDividir() {
		try {
			calculadora.dividir(0);
			fail("Deberia lanzar una excepcion.");
		} catch (ArithmeticException e) {
		}
	}

	public void testResta() {
		calculadora.sumar(100);
		calculadora.restar(20);
		assertEquals(80, calculadora.getCalculo());
		assertTrue((calculadora.getCalculo() == 80));
	}

	public static void main(String[] args) {
		// Modo texto
		// junit.textui.TestRunner.run(CalculadoraTest.class);
		// Modo grafico con swing
		TestRunner.run(CalculadoraTest.class);
		// Modo grafico con awt
		// junit.awtui.TestRunner.run(CalculadoraTest.class);
	}

}
