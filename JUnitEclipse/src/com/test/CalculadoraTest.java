/**
 * 
 */
package com.test;

import com.calculadora.Calculadora;

import junit.framework.TestCase;

/**
 * @author Alex
 *
 */
public class CalculadoraTest extends TestCase {
	
	private Calculadora calculadora = null;

	/**
	 * @param name
	 */
	public CalculadoraTest(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		calculadora = new Calculadora();
		calculadora.inicializar();
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test method for {@link com.calculadora.Calculadora#sumar(int)}.
	 */
	public void testSumar() {
		this.calculadora.sumar(2);
		this.assertTrue(this.calculadora.getCalculo() == 2);
	}

	/**
	 * Test method for {@link com.calculadora.Calculadora#restar(int)}.
	 */
	public void testRestar() {
		calculadora.sumar(10);
		calculadora.restar(2);
		assertEquals(8, calculadora.getCalculo());
	}

}
